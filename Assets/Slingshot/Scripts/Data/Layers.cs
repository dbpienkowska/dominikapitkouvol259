﻿using UnityEngine;

public static class Layers
{
    public const float DEFAULT = 0;
    public const float TRANSPARENT_FX = 1;
    public const float IGNORE_RAYCAST = 2;
    public const float WATER = 3;
    public const float UI = 4;

    public const float GROUND = 8;

    public static bool Contains(this LayerMask _mask, int _layer)
    {
        return _mask == (_mask | (1 << _layer));
    }
}
