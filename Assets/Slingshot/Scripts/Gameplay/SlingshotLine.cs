﻿using UnityEngine;

public abstract class SlingshotLine : MonoBehaviour
{
    [SerializeField] protected Slingshot slingshot = null;

    protected LineRenderer lineRenderer = null;
    protected bool isDrawing = false;

    protected virtual void Reset()
    {
        slingshot = FindObjectOfType<Slingshot>();
    }

    protected virtual void Awake()
    {
        TryGetComponent(out lineRenderer);
    }

    private void Update()
    {
        if (slingshot == null)
        {
            enabled = false;
            return;
        }

        if (slingshot.IsAiming)
        {
            Draw();
        }
        else
        {
            Erase();
        }
    }

    public abstract void Draw();
    public abstract void Erase();
}