﻿using System.Collections;
using UnityEngine;

public partial class Slingshot : MonoBehaviour
{
    public System.Action OnLoaded = null;
    public System.Action OnFired = null;
    public System.Action<Vector2> OnAiming = null;

    public bool IsAiming => isAiming;
    public Vector2 AimDirection => aimDirection;
    public SlingshotProjectile LoadedProjectile => loadedProjectile;

    public Transform LoadPoint = null;
    public SlingshotProjectile ProjectilePrefab = null;

    public float LoadDelay = 1f;
    public float Strength = 10f;

    private SlingshotProjectile loadedProjectile = null;
    private Vector2 aimDirection = Vector2.zero;
    private Camera mainCamera = null;

    private bool isLoaded = false;
    private bool isAiming = false;

    private Coroutine loadCoroutine = null;

    private void Start()
    {
        if(hasProjectile() == false)
        {
            enabled = false;
        }

        findMainCamera();
        load();
    }

    private void Update()
    {
        if (hasMainCamera() == false)
        {
            enabled = false;
            return;
        }

        if (isLoaded == false)
        {
            return;
        }

        if (isAiming)
        {
            aim();

            if(Input.GetMouseButtonUp(0))
            {
                fire();
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                aim();
            }
        }
    }

    private void OnDestroy()
    {
        if(loadedProjectile != null)
        {
            loadedProjectile.OnHitGround -= onProjectileHitGround;
        }
    }

    private void load()
    {
        if (isLoaded)
        {
            return;
        }

        loadedProjectile = Instantiate(ProjectilePrefab, LoadPoint.position, LoadPoint.rotation);
        loadedProjectile.FreezePhysics();
        isLoaded = true;

        OnLoaded?.Invoke();
    }

    private void loadAfterDelay()
    {
        if(loadCoroutine != null)
        {
            StopCoroutine(loadCoroutine);
        }

        loadCoroutine = StartCoroutine(_loadAfterDelay());

        IEnumerator _loadAfterDelay()
        {
            yield return new WaitForSeconds(LoadDelay);
            load();
        }
    }

    private void aim()
    {
        aimDirection = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        aimDirection = (Vector2)LoadPoint.position - aimDirection;

        isAiming = true;
    }

    private void fire()
    {
        isAiming = false;

        if(loadedProjectile == null)
        {
            return;
        }

        loadedProjectile.OnHitGround += onProjectileHitGround;
        loadedProjectile.Fire(aimDirection * Strength);
        loadedProjectile = null;
        isLoaded = false;

        OnFired?.Invoke();
    }

    private void onProjectileHitGround(SlingshotProjectile _projectile)
    {
        _projectile.OnHitGround -= onProjectileHitGround;
        loadAfterDelay();
    }

    private void findMainCamera()
    {
        mainCamera = Camera.main;
    }

    private bool hasProjectile()
    {
        if (ProjectilePrefab == null)
        {
            printError_ProjectileNotAssigned();
            return false;
        }

        return true;
    }

    private bool hasMainCamera()
    {
        if (mainCamera == null)
        {
            printError_MissingCamera();
            return false;
        }

        return true;
    }

    partial void printError_MissingCamera();
    partial void printError_ProjectileNotAssigned();
}

#if UNITY_EDITOR || DEVELOPMENT_BUILD
public partial class Slingshot
{
    partial void printError_MissingCamera()
    {
        Debug.LogError($"MainCamera is missing - disabling Slinshot! (Slingshot: {name})", gameObject);
    }

    partial void printError_ProjectileNotAssigned()
    {
        Debug.LogError($"Projectile for Slingshot is not assigned - disabling Slinshot! (Slingshot: {name})", gameObject);
    }
}
#endif
