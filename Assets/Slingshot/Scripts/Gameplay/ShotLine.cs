﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class ShotLine : SlingshotLine
{
    public int PointCount = 30;
    public float TimeStep = 0.05f;

    protected override void Awake()
    {
        base.Awake();

        lineRenderer.positionCount = PointCount;
    }

    public override void Draw()
    {
        lineRenderer.positionCount = PointCount;

        Vector2 _startPosition = slingshot.LoadPoint.position;
        Vector2 _velocity = slingshot.AimDirection * slingshot.Strength / slingshot.LoadedProjectile.Body.mass;
        Vector2 _position = _startPosition;

        for (int i = 0; i < PointCount; i++)
        {
            float _t = TimeStep * i;
            float _x = _velocity.x * _t;
            float _y = _velocity.y * _t - Physics2D.gravity.y * _t * _t / 2f;

            lineRenderer.SetPosition(i, _position);

            _velocity += Physics2D.gravity * TimeStep;
            _position = _startPosition + new Vector2(_x, _y);
        }
    }

    public override void Erase()
    {
        for (int i = 0; i < PointCount; i++)
        {
            lineRenderer.SetPosition(i, Vector3.zero);
        }
    }
}
