﻿using System.Collections;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Slingshot slingshot = null;

    public AnimationCurve cameraReturnCurve;
    public float ReturnDelay = 1f;
    public float ReturnDuration = 0.5f;

    private Transform mainCamera = null;
    private Transform projectileTransform = null;

    private Vector3 initPosition = Vector3.zero;
    private float offset = 0f;

    private bool isFollowing = false;

    private void Reset()
    {
        slingshot = FindObjectOfType<Slingshot>();
    }

    private void Awake()
    {
        mainCamera = Camera.main.transform;
        initPosition = mainCamera.position;

        SlingshotProjectile.OnProjectileHitGround += onProjectileGrounded;

        slingshot.OnFired += onFired;
        slingshot.OnLoaded += onLoaded;
    }

    private void Update()
    {
        if (isFollowing)
        {
            Vector3 _position = mainCamera.position;
            _position.x = projectileTransform.transform.position.x - offset;
            mainCamera.position = _position;
        }
    }

    private void OnDestroy()
    {
        SlingshotProjectile.OnProjectileHitGround -= onProjectileGrounded;

        if(slingshot != null)
        {
            slingshot.OnFired -= onFired;
            slingshot.OnLoaded -= onLoaded;
        }
    }

    public void onLoaded()
    {
        projectileTransform = slingshot.LoadedProjectile.transform;
    }

    public void onFired()
    {
        isFollowing = true;
        offset = projectileTransform.position.x - mainCamera.position.x;
    }

    private void onProjectileGrounded()
    {
        isFollowing = false;
        StartCoroutine(ReturnCoroutine());
    }

    private IEnumerator ReturnCoroutine()
    {
        float _startX = mainCamera.position.x;
        float _endX = initPosition.x;

        yield return new WaitForSeconds(ReturnDelay);

        float _time = 0f;

        while (_time < ReturnDuration)
        {
            _time += Time.deltaTime;

            Vector3 position = mainCamera.position;
            position.x = Mathf.Lerp(_startX, _endX, cameraReturnCurve.Evaluate(_time / ReturnDuration));
            mainCamera.position = position;

            yield return null;
        }

        mainCamera.position = initPosition;
    }
}
