﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class StringLine : SlingshotLine
{
    public override void Draw()
    {
        Vector2 _startPosition = slingshot.LoadPoint.position;

        lineRenderer.SetPosition(0, _startPosition);
        lineRenderer.SetPosition(1, _startPosition - slingshot.AimDirection);

        isDrawing = true;
    }

    public override void Erase()
    {
        if (isDrawing == false)
        {
            return;
        }

        lineRenderer.SetPosition(0, Vector2.zero);
        lineRenderer.SetPosition(1, Vector2.zero);

        isDrawing = false;
    }
}
