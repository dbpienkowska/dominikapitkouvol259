﻿using UnityEngine;

public class SlingshotProjectile : MonoBehaviour
{
    public Rigidbody2D Body => body;

    public static event System.Action OnProjectileHitGround = null;
    public event System.Action<SlingshotProjectile> OnHitGround = null;

    private Rigidbody2D body = null;

    private void Awake()
    {
        findBody();
    }

    public void Fire(Vector2 _force)
    {
        if(body == null)
        {
            return;
        }

        body.simulated = true;
        body.AddForce(_force, ForceMode2D.Impulse);
    }

    public void FreezePhysics()
    {
        if (body == null)
        {
            return;
        }

        body.simulated = false;
    }

    private void OnCollisionEnter2D(Collision2D _collision)
    {
        if(_collision.gameObject.layer == Layers.GROUND)
        {
            OnHitGround?.Invoke(this);
            OnProjectileHitGround?.Invoke();
        }
    }

    private void findBody()
    {
        body = GetComponentInChildren<Rigidbody2D>();
    }
}
